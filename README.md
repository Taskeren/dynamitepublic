# Dynamite

Dynamite is a rhythm game made by TunerGames, and was taken down by the CN government.

## Dyfused

Dyfused is an organization working on Dynamite since the takedown. We've created Explode series to provide a server service like it was.

### ExplodeX

The latest server implementation, providing all functions that the official one provided.

### Dynamite Dyfused

It was a branch of Dynamite, providing continuous updates in multiserver improvement and bug fixes. But it was terminated because the developers received the warning message from TunerGames. Also the repository was fully removed as they asked.
